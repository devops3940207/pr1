# Pr1
Первая практическая работа по дисциплине DevOps.

## Шаги
1. Клонируем репозиторий
```shell
git clone <repository_url>
```
2. Добавляем права на запуск `build.sh`
```shell
sudo chmod +x build.sh 
```
3. Запускаем скрипт `build.sh`
```shell
./build.sh
```
4. На выходе получаем deb пакет в папке `build`, который можно установить с помощью команды:
```shell
sudo dpkg -i build/pr1-1.0.0-Linux.deb
```
Теперь код можно запустить командой `pr1`